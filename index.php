<?php include('head.html'); ?>

<nav id="page-nav" class="navbar navbar-inverse">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#page-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <span class="navbar-brand" href="index.php">
            <img src="img/Primeage-header.png" class="img-responsive">
        </span>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div id="fixed-nav">
        <div class="collapse navbar-collapse" id="page-navbar-collapse">
            <div class="container">
                <ul class="nav nav-justified">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="about.php">About Us</a></li>
                    <li><a href="services.php">Services</a></li>
                    <li><a href="membership.php">Membership</a></li>
                    <li><a href="#contact-us">Contact Us</a></li>
                </ul>
            </div><!-- /.container -->
        </div><!-- /.navbar-collapse -->
    </div>
</nav>

<section id="intro-img-lg" class="landing-page">
    <div id="intro-video"></div>
    <div id="intro-carousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="img/banners__inset-home-1.jpg" alt="">
                <div class="carousel-caption">
                    <div id="intro-text">
                        <div class="container">
                            <h2 class="caption-title">JUMP. LIFT. RUN. CYCLE</h2>
                            <p>Sounds simple, right? Well, that's all you need to restore and sustain your God given
                                Beauty and Health. We are here to help you do just that. Each of our exercises is
                                designed to keep you fit, young and healthy.</p>
                            <p>Stay young. Stay healthy</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('footer.html'); ?>
