<?php include('head.html'); ?>

	<nav id="page-nav" class="navbar navbar-inverse">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#page-navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="img/Primeage-header.png" class="img-responsive">
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="page-navbar-collapse">
			<div class="container">
				<ul class="nav nav-justified">
					<li><a href="index.php">Home</a></li>
					<li class="active"><a href="about.php">About Us</a></li>
					<li><a href="services.php">Services</a></li>
					<li><a href="membership.php">Membership</a></li>
					<li><a href="#contact-us">Contact Us</a></li>
				</ul>
			</div><!-- /.container -->
		</div><!-- /.navbar-collapse -->
	</nav>

	<section id="intro-img-lg" class="inset-page">
		<div id="intro-img">
			<img src="img/banners__inset-about.jpg" class="img-responsive">
		</div>
	</section>

	<div id="page-wrap">
		<section id="our-story">
			<div class="container">
				<div class="row well-lg clear">
					<div class="col-md-10 col-md-offset-1">
						<p>PRIME AGE BEAUTY AND FITNESS CENTRE is a unique venture born out of the need to provide quality affordable services to a population laden with lifestyle challenges. Our sole aim of getting in to the business is to sustain as well as restore mankind’s God given Beauty and Health.</p>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<h4>Mission</h4>
						<p>Taking care of human kind through quality restoration(promotion) of beauty and fitness necessary for healthy, enjoyable living.</p>
						<h4>Vision</h4>
						<p>To be the standard of reference in beauty and fitness services.</p>
					</div>

					<div class="col-md-6">
						<h4>Core Values</h4>
						<div class="row">
							<div class="col-sm-4">
								<ul class="unordered-list">
									<li>Integrity</li>
									<li>Honest</li>
									<li>Truthful</li>
								</ul>
							</div>
							<div class="col-sm-4">
								<ul class="unordered-list">
									<li>Trustworthy</li>
									<li>Accountable</li>
									<li>Dependable</li>
								</ul>
							</div>
							<div class="col-sm-4">
								<ul class="unordered-list">
									<li>Reliable</li>
									<li>Respectful</li>
									<li>Passionate</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

<?php include('footer.html'); ?>
