<?php include('head.html'); ?>

<nav id="page-nav" class="navbar navbar-inverse">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#page-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">
            <img src="img/Primeage-header.png" class="img-responsive">
        </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="page-navbar-collapse">
        <div class="container">
            <ul class="nav nav-justified">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About Us</a></li>
                <li class="active"><a href="services.php">Services</a></li>
                <li><a href="membership.php">Membership</a></li>
                <li><a href="#contact-us">Contact Us</a></li>
            </ul>
        </div><!-- /.container -->
    </div><!-- /.navbar-collapse -->
</nav>

<section id="intro-img-lg" class="inset-page">
    <div id="intro-img">
        <img src="img/banners__inset-services.jpg" class="img-responsive">
    </div>
</section>

<div id="page-wrap" class="container">
    <div class="col-md-10 col-md-offset-1">
        <section id="intro">
            <div class="well-lg clear">
                    <p>At primage gym we offer a range of services to suit every member’s need. We have pro trainer who will
                        customize a workout regime according to your needs and fitness level.</p>
            </div>
        </section>

        <nav id="list-block">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#aerobics" aria-controls="aerobics" role="tab" data-toggle="tab">Aerobics</a>
                </li>
                <li role="presentation">
                    <a href="#cardio" aria-controls="cardio" role="tab" data-toggle="tab">Cardio</a>
                </li>
                <li role="presentation">
                    <a href="#strength-training" aria-controls="strength-training" role="tab" data-toggle="tab">Strength Training</a>
                </li>
                <li role="presentation">
                    <a href="#toning" aria-controls="toning" role="tab" data-toggle="tab">Toning</a>
                </li>
            </ul>
        </nav>

        <!-- Tab panes -->
        <article class="tab-content">
            <section role="tabpanel" class="tab-pane well-lg clear active" id="aerobics">
                <p>Aerobics is any physical activity that causes you to sweat, increase your heart rate and cause you to
                    breathe faster. It’s meant to train your cardiovascular system to manage and deliver oxygen efficiently
                    throughout your body. Benefits of this exercise include weight loss, stronger heart, lower risk of
                    chronic disease and coronary heart disease and improves sleep. Our trainers will guide you in a rhythmic
                    aerobics classes allowing participant select their level of participation depending on their fitness
                    level.</p>
            </section>
            <section role="tabpanel" class="tab-pane well-lg clear" id="cardio">
                <p>Cardiovascular exercise is any type of exercise that increases the work of the heart and lungs. Some of
                    the benefits of cardio are: improves and strengthens cardiovascular system, helps increase blood
                    circulation, helps lower blood pressure and cholesterol at the same time burning calories and aiding in
                    weight loss. Our gym is equipped with a variety of machines for cardio exercise.</p>
            </section>
            <section role="tabpanel" class="tab-pane well-lg clear" id="strength-training">
                <p>This exercise is meant to stress the body usually with weights, resistance bands and body weight in order
                    to increase muscle size, strength and endurance. Strength training helps develop strong bones, manage
                    weight, manage chronic conditions. Our instructors will guide you on core work and weight lifting.</p>
            </section>
            <section role="tabpanel" class="tab-pane well-lg clear" id="toning">
                <p>This exercise main aim is to develop a physique with the main focus being on musculature. It’s meant to
                    make your body leaner with noticeable muscle definition and shape but it doesn’t give muscle size like
                    strength training. Our dedicated team with help you guide you in your toning exercises such as
                    resistance training, cardiovascular exercise and weights.</p>
            </section>
        </article>
    </div>

    <!--		<section id="intro-img-sm">-->
    <!--			<div id="intro-text">-->
    <!--				<div class="container">-->
    <!--					<h1>Services</h1>-->
    <!--					<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis lectus metus, at posuere neque. Sed pharetra nibh eget orci convallis at posuere leo convallis.</p>-->
    <!--				</div>-->
    <!--			</div>-->
    <!---->
    <!--			<div id="intro-img">-->
    <!--				<img src="img/Primeage-Beauty-and-Fitness-Gym.jpg" class="img-responsive">-->
    <!--			</div>-->
    <!--		</section>-->
    <!-- Nav tabs -->

</div>


<?php include('footer.html'); ?>
