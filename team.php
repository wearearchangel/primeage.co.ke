<?php include('head.html'); ?>

	<section class="container-fluid">
		<div id="intro-img-lg">
			<div id="intro-img" class="hidden-xs">
				<img src="img/placeholder-full.jpg" class="img-responsive">
			</div>
		</div>

		<header class="row">
			<nav id="landing-nav" class="col-sm-12">
				<ul class="nav nav-pills nav-justified">
					<li><a href="index.php">Home</a></li>
					<li class="active"><a href="about.php">About Us</a></li>
					<li><a href="services.php">Services</a></li>
					<li><a href="membership.php">Membership</a></li>
					<li><a href="contact.php">Contact Us</a></li>
				</ul>
			</nav>
		</header>

		<section class="row">
			<div class="col-sm-12">
				<div id="intro-text">
					<h1>Team</h1>
					<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</div>

				<div id="intro-img-sm">
					<div id="intro-img" class="visible-xs">
						<img src="img/placeholder-full.jpg" class="img-responsive">
					</div>
				</div>
			</div>
		</section>

		<section class="row">
			<div class="col-sm-12">
				<nav id="list-accordion">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#">Person 1</a></li>
						<li><a href="#">Person 2</a></li>
						<li><a href="#">Person 3</a></li>
					</ul>
				</nav>
			</div>
		</section>
	</section>

<?php include('footer.html'); ?>
