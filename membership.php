<?php include('head.html'); ?>

	<nav id="page-nav" class="navbar navbar-inverse">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#page-navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="img/Primeage-header.png" class="img-responsive">
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="page-navbar-collapse">
			<div class="container">
				<ul class="nav nav-justified">
					<li><a href="index.php">Home</a></li>
					<li><a href="about.php">About Us</a></li>
					<li><a href="services.php">Services</a></li>
					<li class="active"><a href="membership.php">Membership</a></li>
					<li><a href="#contact-us">Contact Us</a></li>
				</ul>
			</div><!-- /.container -->
		</div><!-- /.navbar-collapse -->
	</nav>

	<div id="page-wrap" class="jumbotron clear">
		<section id="membership" class="container">
			<table class="table">
				<thead>
					<tr>
						<th></th>
						<th colspan="2">Single</th>
						<th colspan="2">Family of 2 <span>Discount Rate: 8%</span></th>
						<th colspan="2">Family of 3 <span>Discount Rate: 10%</span></th>
						<th colspan="2">Family of 4 <span>Discount Rate: 12%</span></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<th class="t__anchor">Peak time</th>
						<th>Off Peak</th>
						<th class="t__anchor">Peak time</th>
						<th>Off Peak</th>
						<th class="t__anchor">Peak time</th>
						<th>Off Peak</th>
						<th class="t__anchor">Peak time</th>
						<th>Off Peak</th>
					</tr>
					<tr>
						<th>Monthly <span>1 month</span></th>
						<td class="t__anchor">3,000</td>
						<td>2,600</td>
						<td class="t__anchor">5,500</td>
						<td>4,700</td>
						<td class="t__anchor">8,100</td>
						<td>6,900</td>
						<td class="t__anchor">10,600</td>
						<td>9,000</td>
					</tr>
					<tr>
						<th>Quarterly <span>3 months</span></th>
						<td class="t__anchor">8,600</td>
						<td>7,400</td>
						<td class="t__anchor">15,800</td>
						<td>13,400</td>
						<td class="t__anchor">23,200</td>
						<td>19,700</td>
						<td class="t__anchor">30,300</td>
						<td>25,800</td>
					</tr>
					<tr>
						<th>Semi Annual <span>6 months</span></th>
						<td class="t__anchor">16,600</td>
						<td>14,400</td>
						<td class="t__anchor">30,500</td>
						<td>25,900</td>
						<td class="t__anchor">44,800</td>
						<td>38,100</td>
						<td class="t__anchor">58,400</td>
						<td>49,600</td>
					</tr>
					<tr>
						<th>Annually <span>12 months</span></th>
						<td class="t__anchor">32,400</td>
						<td>28,100</td>
						<td class="t__anchor">59,600</td>
						<td>50,800</td>
						<td class="t__anchor">87,500</td>
						<td>74,400</td>
						<td class="t__anchor">114,000</td>
						<td>96,900</td>
					</tr>
				</tbody>
			</table>
			<p>&nbsp;</p>
			<p>Daily Rate: Kes. 300 per person</p>
			<h4>Note:</h4>
			<ul>
				<li>Subscribers entitled to a maximum of 4 days per week</li>
				<li>Peak-time subscribers can enjoy their subscription during off-peak hours but no credit is given</li>
				<li>Off-peak subscribers cannot attend during peak-time</li>
				<li>Daily rate is only available at off-peak hours</li>
			</ul>
		</section>
	</div>

<?php include('footer.html'); ?>
