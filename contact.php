<?php include('head.html'); ?>

	<nav id="page-nav" class="navbar navbar-inverse">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#page-navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="img/Primeage-header.png" class="img-responsive">
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="page-navbar-collapse">
			<div class="container">
				<ul class="nav nav-justified">
					<li><a href="index.php">Home</a></li>
					<li><a href="about.php">About Us</a></li>
					<li><a href="services.php">Services</a></li>
					<li><a href="membership.php">Membership</a></li>
					<li class="active"><a href="contact.php">Contact Us</a></li>
				</ul>
			</div><!-- /.container -->
		</div><!-- /.navbar-collapse -->
	</nav>

	<div id="page-wrap" class="jumbotron clear">
		<section class="container">
			<div class="col-sm-8">
				<address>
					<h4>We are located at</h4>
					Kenyatta Road, Next to Muigai Administrative Police(A.P.) Post<br>
					<abbr title="Phone">P:</abbr> (+254) 722 674213<br>
					<abbr title="Email">E:</abbr> <a href="mailto:#">info@primeage.co.ke</a>
				</address>
			</div>

			<div class="col-sm-4">
				<h4>We are open:</h4>
				Weekdays - 5am to 9pm<br>
				Saturdays - 7am to 7pm<br>
				Sundays - 11am to 7pm</p>
			</div>
		</section>
	</div>

<?php include('footer.html'); ?>
