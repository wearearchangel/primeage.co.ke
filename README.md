# Prime Age Template #
This is the official template for Prime Age Beauty and Fitness Centre, Kenyatta Road

### How do I get set up? ###
* Make sure you have a working version of WordPress
* Download the template onto the wp-content/themes folder
* Make this theme the default

[![Deploy to Azure](http://azuredeploy.net/deploybutton.png)](https://azuredeploy.net/)