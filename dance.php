<?php include('head.html'); ?>

	<nav id="page-nav" class="navbar navbar-inverse">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#page-navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="img/Primeage-header.png" class="img-responsive">
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="page-navbar-collapse">
			<div class="container">
				<ul class="nav nav-justified">
					<li><a href="index.php">Home</a></li>
					<li><a href="about.php">About Us</a></li>
					<li class="active"><a href="services.php">Services</a></li>
					<li><a href="membership.php">Membership</a></li>
					<li><a href="contact.php">Contact Us</a></li>
				</ul>
			</div><!-- /.container -->
		</div><!-- /.navbar-collapse -->
	</nav>

	<section id="intro-img-lg" class="sub-page hidden-xs">
		<div id="intro-img">
			<img src="img/gym.jpg" class="img-responsive">
		</div>

		<div id="intro-text">
			<div class="container">
				<h1>Services</h1>
			</div>
		</div>
	</section>

	<nav id="list-block" class="navbar">
		<ul class="nav nav-tabs nav-justified">
			<li><a href="fitness.php">
				<i class="glyphicon glyphicon-home"></i> Fitness</a>
			</li>
			<li><a href="management.php">
				<i class="glyphicon glyphicon-home"></i> Management</a>
			</li>
			<li class="active"><a href="dance.php">
				<i class="glyphicon glyphicon-home"></i> Dance</a>
			</li>
			<li><a href="martial-arts.php">
				<i class="glyphicon glyphicon-home"></i> Martial Arts</a>
			</li>
			<li><a href="beauty.php">
				<i class="glyphicon glyphicon-home"></i> Beauty</a>
			</li>
		</ul>
	</nav>

	<div id="page-wrap" class="jumbotron clear">
		<section class="container">
			<div class="col-md-6">
				Video
			</div>
			<div class="col-md-6">
				<p>Dance is an Art. It involves movement of the body usually rythmically. It takes music appreciation and Choreography to master the art.</p>
				<p>With some of the best Instructors in the country we offer a variety from the Foundations of Dance to the more adnvanced stage
Ballet, Salsa, Traditional, Modern, Thearitical & Choreographed Dance for Individuals and Groups.</p>
			</div>
		</section>
	</div>

<?php include('footer.html'); ?>
