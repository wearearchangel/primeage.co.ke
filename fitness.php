<?php include('head.html'); ?>

	<nav id="page-nav" class="navbar navbar-inverse">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#page-navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">
				<img src="img/Primeage-header.png" class="img-responsive">
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="page-navbar-collapse">
			<div class="container">
				<ul class="nav nav-justified">
					<li><a href="index.php">Home</a></li>
					<li><a href="about.php">About Us</a></li>
					<li class="active"><a href="services.php">Services</a></li>
					<li><a href="membership.php">Membership</a></li>
					<li><a href="contact.php">Contact Us</a></li>
				</ul>
			</div><!-- /.container -->
		</div><!-- /.navbar-collapse -->
	</nav>

	<section id="intro-img-lg" class="sub-page hidden-xs">
		<div id="intro-img">
			<img src="img/gym.jpg" class="img-responsive">
		</div>

		<div id="intro-text">
			<div class="container">
				<h1>Services</h1>
			</div>
		</div>
	</section>

	<nav id="list-block" class="navbar">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a href="fitness.php">
				<i class="glyphicon glyphicon-home"></i> Fitness</a>
			</li>
			<li><a href="management.php">
				<i class="glyphicon glyphicon-home"></i> Management</a>
			</li>
			<li><a href="dance.php">
				<i class="glyphicon glyphicon-home"></i> Dance</a>
			</li>
			<li><a href="martial-arts.php">
				<i class="glyphicon glyphicon-home"></i> Martial Arts</a>
			</li>
			<li><a href="beauty.php">
				<i class="glyphicon glyphicon-home"></i> Beauty</a>
			</li>
		</ul>
	</nav>

	<nav id="inset-nav" class="navbar navbar-default" data-spy="scroll">
		<div class="container">
			<ul class="nav">
				<li class="active"><a href="#fitness">Why get fit</a></li>
				<li><a href="#aerobics">Aerobics</a></li>
				<li><a href="#strength-training">Strength Training</a></li>
			</ul>
		</div>
	</nav>

	<div id="page-wrap">
		<section id="fitness" class="jumbotron clear">
			<div class="container">
				<div class="col-md-6">
					Video
				</div>
				<div class="col-md-6">
					<h3>Why should you get fit?</h3>
					<ul class="unordered-list">
						<li>Working out keeps both the body alert and the brain active, vital for people of all age.</li>
						<li>Strenthg Training offers a way to improve strength and flexibility, which helps keeps muscles and joints healthy</li>
						<li>Group Workouts are a good start to a new hobby that is helpful, making you meet new people and make friends with those who have similar interests.</li>
						<li>Dance helps you learn about your body, improving your posture and balance</li>
						<li>Taking time out in the day to exercise not only leaves you fit but also helps in reducing stress levels</li>
						<li>Fitness can led to new career opportunities, or help build vital communications skills needed in every profession</li>
						<li>Being Fit  helps increase your self esteem and confidence through mastering new behaviours and skills</li>
						<li>The Gym offers creative opportunities for people to express their personalities and learn from others.</li>
						<li>Workingout is an awsome way to open up new possibilities, keep healthy and  beat stress while having fun at it.</li>
					</ul>
					<p>Don’t have a plan? We got a selection to pick from, enabling you to Keep Fit while Having Fun.</p>
				</div>
			</div>
		</section>

		<section id="aerobics" class="jumbotron spaced">
			<div class="container">
				<div class="col-md-10 col-md-offset-1 text-center">
					<h1 class="title">Aerobics</h1>
					<p>Aerobics is any physical activity that causes you to sweat, increase your heart rate and cause you to breathe faster. It’s meant to train your cardiovascular system to manage and deliver oxygen efficiently throughout your body. Benefits of this exercise include weight loss, stronger heart, lower risk of chronic disease and coronary heart disease and improves sleep. Our trainers will guide you in a rhythmic aerobics classes allowing participant select their level of participation depending on their fitness level.</p>
				</div>
				<p class="clearfix"></p>
				<table class="table">
					<thead>
						<tr>
							<th>Day</th>
							<th colspan="2">Time</th>
							<th>Event</th>
							<th>Coach</th>
						</tr>
						<tr>
							<th></th>
							<th>6am - 7am</th>
							<th>6pm - 7pm</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Monday</td>
							<td></td>
							<td><i class="glyphicon glyphicon-ok"></i></td>
							<td>Aerobics Dance</td>
							<td>Davi</td>
						</tr>
						<tr>
							<td>Tuesday</td>
							<td></td>
							<td><i class="glyphicon glyphicon-ok"></i></td>
							<td>Taebo</td>
							<td>Alfa</td>
						</tr>
						<tr>
							<td>Wednesday</td>
							<td></td>
							<td><i class="glyphicon glyphicon-ok"></i></td>
							<td>Afro-Zumba</td>
							<td>Viola and Mathenge</td>
						</tr>
						<tr>
							<td>Thursday</td>
							<td></td>
							<td><i class="glyphicon glyphicon-ok"></i></td>
							<td>Circuits and Toning</td>
							<td>Davi</td>
						</tr>
						<tr>
							<td>Friday</td>
							<td></td>
							<td><i class="glyphicon glyphicon-ok"></i></td>
							<td>Step Aerobics</td>
							<td>Alfa</td>
						</tr>
					</tbody>
				</table>
			</div>
		</section>

		<section id="strength-training" class="jumbotron">
			<div class="container">
				<div class="col-md-10 col-md-offset-1">
					<h1 class="title">Cardio</h1>
					<p>Cardiovascular exercise is any type of exercise that increases the work of the heart and lungs. Some of the benefits of cardio are: improves and strengthens cardiovascular system, helps increase blood circulation, helps lower blood pressure and cholesterol at the same time burning calories and aiding in weight loss. Our gym is equipped with a variety of machines for cardio exercise.</p>
				</div>
			</div>
		</section>

		<section id="strength-training" class="jumbotron clear spaced">
			<div class="container">
				<div class="col-md-10 col-md-offset-1">
<!--				<h1 class="title">Strength Training and Conditioning</h1>-->
<!--				<p>This is always associated with huge veins poping out and body building, but it doesn't have to be that way for you to see serious benefits. Make some time to talk to our professionals and realise the maximum benefits. Guess what, it is recommended for ALL GENDERS.</p>-->
<!--				<p>Main focus is on the use of resistance to induce muscular contraction which then builds strength, anaerobic endurance, and depending on the goal,size of the muscles involved.Properly performing this workouts regularly leaves you significant functional benefits and improvement in overall health and well-being, including but not limited to:-->
<!--					<ul class="unordered-list">-->
<!--						<li>increased muscle,</li>-->
<!--						<li>connective tissues,</li>-->
<!--						<li>bone strength and toughness making you easily endure pain.</li>-->
<!--						<li>improved joint function with reduced potential for injury thus safe movement with minimal chances of injuries espicially in old days.</li>-->
<!--						<li>increased bone density, increased metabolism, increased fitness, improved cardiac function, and improved lipoprotein lipid profiles, including elevated HDL ("good") cholesterol, the more reasons it slows aging.</li>-->
<!--					</ul>-->
<!--				</p>-->
					<h1 class="title">Strength training.</h1>
					<p>This exercise is meant to stress the body usually with weights, resistance bands and body weight in order to increase muscle size, strength and endurance. Strength training helps develop strong bones, manage weight, manage chronic conditions. Our instructors will guide you on core work and weight lifting.</p>
				</div>
			</div>
		</section>
	</div>

<?php include('footer.html'); ?>
